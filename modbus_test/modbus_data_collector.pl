#!/usr/bin/env perl
#===============================================================================
#
#         FILE: modbus_data_collector.pl
#
#        USAGE: ./modbus_data_collector.pl  
#
#  DESCRIPTION: Скрипт собирает данные по модбас с ПЛК SMH4 и складывает данные в базу
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.2
#      CREATED: 14.02.2020 11:26:25
#     REVISION: 26.02.2020
#===============================================================================

use strict;
use warnings;
use utf8;
use Device::Modbus::TCP::Client;
use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use modbusfn;
use Data::Dumper;
use Data::Printer;
binmode(STDOUT,':utf8');
use 5.010;

	while (1){
		modbusfn::db_insert_data(modbusfn::get_modbus_data(modbusfn::db_get_modbus_vars));
		sleep(5);
	}
