#!/usr/bin/env perl
#===============================================================================
#
#         FILE: smh4_modbus_test.pl
#
#        USAGE: ./smh4_modbus_test.pl  
#
#  DESCRIPTION: Скрипт для отработки соединения с ПЛК SMH4
#               По modbus получаем значение переменной из тестового проекта для ПЛК
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 27.08.2019 14:29:23
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Device::Modbus::TCP::Client;
use Data::Dumper;
use Data::Printer;
binmode(STDOUT,':utf8');
use 5.010;
use DBI();
use DBD::Pg;

# modbus карта всех регистров ПЛК SMH4  
my %mod_bus_hsh = (     # input_status registers
			"DOUT-Авария(строб)" => ['input_status', 'bool','10000','1','Невозможность работы одного или нескольких контуров (мигание лампы)'],
			"DOUT-Авария" => ['input_status', 'bool','10001','1','Невозможность работы одного или нескольких контуров'],
			"DOUT-Неисправность" => ['input_status', 'bool','10002','1','Неполадки в работе одного или нескольки[ контуров]'],
			"DOUT-НД" => ['input_status', 'bool','10003','1','Включение дренажного насоса'],
			"DOUT-K1-Неисправность" => ['input_status', 'bool','10004','1','Неполадки в работе контура 1'],
			"DOUT-K1-Авария" => ['input_status', 'bool','10005','1','Невозможность работы контура 1'],
			"On_M1.2" => ['input_status', 'bool','10006','1','работает 1 насос контура 2 '],
			"On_M2.2" => ['input_status', 'bool','10007','1','работает 2 насос контура 2 '],
			"Open_Z.2" => ['input_status', 'bool','10008','1','2 задвижка открыта '],
			"Close_Z.2" => ['input_status', 'bool','10009','1','2 задвижка закрыта '],
			"DOUT-K1-НЦ1(звезда)" => ['input_status', 'bool','10010','1','Пусковой режим первого циркуляционного насоса'],
			"DOUT-K1-НЦ1(треугольник)" => ['input_status', 'bool','10011','1','Рабочий режим первого циркуляционного насоса'],
			"DOUT-K1-НЦ2(звезда)" => ['input_status', 'bool','10013','1','Пусковой режим второго циркуляционного насоса'],
			"DOUT-K1-НЦ2(треугольник)" => ['input_status', 'bool','10014','1','Рабочий режим второго циркуляционного насоса'],
			"DOUT-K2-Неисправность" => ['input_status', 'bool','10016','1','Неполадки в работе контура 2'],
			"DOUT-K2-Авария" => ['input_status', 'bool','10015','1','Невозможность работы контура 2'],
			"On_M2.1" => ['input_status', 'bool','10018','1','работает 2 насос контура 1 '],
			"On_M1.1" => ['input_status', 'bool','10019','1','работает 1 насос контура 1 '],
			"Open_Z.1" => ['input_status', 'bool','10017','1','1 задвижка открыта '],
			"Close_Z.1" => ['input_status', 'bool','10012','1','1 задвижка закрыта '],
			"Режим насосов ГВС" => ['input_status', 'bool','10020','1','Ручной - Авто'],
			"DOUT-K2-НЦ1(звезда)" => ['input_status', 'bool','10021','1','Пусковой режим первого циркуляционного насоса'],
			"DOUT-K2-НЦ1(треугольник)" => ['input_status', 'bool','10022','1','Рабочий режим первого циркуляционного насоса'],
			"Сухой ход ГВС" => ['input_status', 'bool','10023','1','сухой ход работы насоса'],
			"DOUT-K2-НЦ2(звезда)" => ['input_status', 'bool','10024','1','Пусковой режим второго циркуляционного насоса'],
			"DOUT-K2-НЦ2(треугольник)" => ['input_status', 'bool','10025','1','Рабочий режим второго циркуляционного насоса'],
			"Термистор M1 K1" => ['input_status', 'bool','10026','1','насос выключен от перегрева'],
			"Термистор M2 K1" => ['input_status', 'bool','10027','1','насос выключен от перегрева'],
			"Режим насосов отопления" => ['input_status', 'bool','10028','1','Ручной - Авто'],
			"Сухой ход ГВС" => ['input_status', 'bool','10029','1','сухой ход работы насоса'],
			"Термистор M1 K2" => ['input_status', 'bool','10030','1','насос выключен от перегрева'],
			"Термистор M2 K2" => ['input_status', 'bool','10031','1','насос выключен от перегрева'],
			"Подтверждение включения Н1" => ['input_status', 'bool','10032','1','разрешение на запуск насоса'],
			"Подтверждение включения Н2" => ['input_status', 'bool','10033','1','разрешение на запуск насоса'],
			"Подтверждение включения Н3" => ['input_status', 'bool','10034','1','разрешение на запуск насоса'],
			"Подтверждение включения Н3" => ['input_status', 'bool','10035','1','разрешение на запуск насоса'],
			"Фазировка питания" => ['input_status', 'bool','10036','1','нарушена последовательность фаз питания'],
			# coil registers
			"DIN-НД(термореле)" => ['coil', 'bool','00001','1','отказ дренажного насоса'],
			"DIN-Доступ в помещение" => ['coil', 'bool','00002','1','датчик проникновения в помещение ИТП'],
			"DIN-Пожар" => ['coil', 'bool','00003','1','сигнал "Пожар"'],
			"STOPSMH-2Gi" => ['coil', 'bool','00004','1','остановка выполнения программы'],
			"4SMH2Gi" => ['coil', 'bool','00006','1','хз'],
			"DIN-K1-Датчик протока" => ['coil', 'bool','00007','1','наличие проока в контуре 1'],
			"DIN-K2-НЦ2(авто)" => ['coil', 'bool','00009','1','наличие проока в контуре 1'],
			"3SMH2Gi" => ['coil', 'bool','00010','1','хз'],
			"2SMH2Gi" => ['coil', 'bool','00011','1','хз'],
			"1SMH2Gi" => ['coil', 'bool','00012','1','хз'],
			"0SMH2Gi" => ['coil', 'bool','00013','1','хз'],
			"DIN-K2-Датчик протока" => ['coil', 'bool','00015','1','наличие проока в контуре 2'],
			"DIN-ТС-Перепад давления" => ['coil', 'bool','00016','1','наличие перепада давления на входе в ИТП'],
			"DIN-K1-Пуск-Стоп" => ['coil', 'bool','00022','1','флажок "Пуск/Стоп" для контура 1'],
			"DIN-K2-НЦ2(авто)" => ['coil', 'bool','00023','1','разрешение запуска и управления вторым циркуляционным насосом контура 1'],
			"DIN-K2-Пуск-Стоп" => ['coil', 'bool','00031','1','флажок "Пуск/Стоп" для контура 2'],
			"DIN-K2-НЦ1(авто)" => ['coil', 'bool','00032','1','разрешение запуска и управления первым циркуляционным насосом контура 2'],
			"DIN-K1-НЦ1(авто)" => ['coil', 'bool','00040','1','разрешение запуска и управления первым циркуляционным насосом контура 1'],
			# input registers
			"VID" => ['input', 'long','0','2','хз'],
			"HW" => ['input', 'long','2','2','хз'],
			"PID" => ['input', 'long','00004','2','хз'],
			"SW" => ['input', 'long','00006','2','хз'],
			"State1" => ['input', 'long','00008','2','хз'],
			"State2" => ['input', 'long','00010','2','хз'],
			"State3" => ['input', 'long','00012','2','хз'],
			"State4" => ['input', 'long','00014','2','хз'],
			"State5" => ['input', 'long','00016','2','хз'],
			"Season" => ['input', 'int','00018','1','хз'],
			"WMode" => ['input', 'int','00019','1','хз'],
			"ACode" => ['input', 'long','00020','2','хз'],
			"GComm" => ['input', 'int','00022','1','хз'],
			"Gin" => ['input', 'float','00023','2','хз'],
			"GStat" => ['input', 'int','00025','1','хз'],
			"LComm" => ['input', 'int','00026','1','хз'],
			"LStat" => ['input', 'int','00027','1','хз'],
			"DPStat" => ['input', 'int','00028','1','хз'],
			"Toutside" => ['input', 'float','00029','2','хз'],
			"Toutside_f" => ['input', 'float','00031','2','хз'],
			"cTC_Type" => ['input', 'int','00033','1','хз'],
			"cK1_Type" => ['input', 'int','00034','1','хз'],
			"cK2_Type" => ['input', 'int','00035','1','хз'],
			"cK3_Type" => ['input', 'int','00036','1','хз'],
			"cK4_Type" => ['input', 'int','00037','1','хз'],
			"cK5_Type" => ['input', 'int','00038','1','хз'],
			"r2" => ['input', 'long','00039','2','хз'],
			"r3" => ['input', 'long','00041','2','хз'],
			"r4" => ['input', 'long','00043','2','хз'],
			"r5" => ['input', 'long','00045','2','хз'],
			"r6" => ['input', 'long','00047','2','хз'],
			"TC_Type" => ['input', 'int','00049','1','хз'],
			"TC_VCode" => ['input', 'long','00050','2','хз'],
			"TC_DCode" => ['input', 'int','00052','1','хз'],
			"TC_ADCode" => ['input', 'int','00053','1','хз'],
			"TC_OCode" => ['input', 'int','00054','1','хз'],
			"TC_ACode" => ['input', 'long','00055','2','хз'],
			"TC_Mode" => ['input', 'int','00057','1','хз'],
			"TC_GMode" => ['input', 'int','00058','1','хз'],
			"TC_GMin" => ['input', 'float','00059','2','хз'],
			"TC_GMax" => ['input', 'float','00061','2','хз'],
			"TC_G1x" => ['input', 'int','00063','1','хз'],
			"TC_G2x" => ['input', 'int','00064','1','хз'],
			"TC_G3x" => ['input', 'int','00065','1','хз'],
			"TC_G1y" => ['input', 'int','00066','1','хз'],
			"TC_G2y" => ['input', 'int','00067','1','хз'],
			"TC_G3y" => ['input', 'int','00068','1','хз'],
			"TC_Gbias" => ['input', 'float','00069','2','хз'],
			"TC_Gout" => ['input', 'float','00071','2','хз'],
			"TC_T1" => ['input', 'float','00073','2','хз'],
			"TC_P1" => ['input', 'float','00075','2','хз'],
			"TC_T2" => ['input', 'float','00077','2','хз'],
			"TC_P2" => ['input', 'float','00079','2','хз'],
			"TC_PP" => ['input', 'int','00081','1','хз'],
			"TC_r1" => ['input', 'long','00082','2','хз'],
			"TC_r2" => ['input', 'long','00084','2','хз'],
			"TC_r3" => ['input', 'long','00086','2','хз'],
			"TC_r4" => ['input', 'long','00088','2','хз'],
			"TC_r5" => ['input', 'long','00090','2','хз'],
			"K1_Type" => ['input', 'int','00092','1','хз'],
			"K1_VCode" => ['input', 'long','00093','2','хз'],
			"K1_DCode" => ['input', 'int','00095','1','хз'],
			"K1_ADCode" => ['input', 'int','00096','1','хз'],
			"K1_OCode" => ['input', 'int','00097','1','хз'],
			"K1_ACode" => ['input', 'long','00098','2','хз'],
			"K1_Mode" => ['input', 'int','00100','1','хз'],
			"K1_GMode" => ['input', 'int','00101','1','хз'],
			"K1_GMin" => ['input', 'float','00102','2','хз'],
			"K1_GMax" => ['input', 'float','00104','2','хз'],
			"K1_G1x" => ['input', 'int','00106','1','хз'],
			"K1_G2x" => ['input', 'int','00107','1','хз'],
			"K1_G3x" => ['input', 'int','00108','1','хз'],
			"K1_G1y" => ['input', 'int','00109','1','хз'],
			"K1_G2y" => ['input', 'int','00110','1','хз'],
			"K1_G3y" => ['input', 'int','00111','1','хз'],
			"K1_Gbias" => ['input', 'float','00112','2','хз'],
			"K1_Gout" => ['input', 'float','00114','2','хз'],
			"K1_T3" => ['input', 'float','00116','2','хз'],
			"K1_P3" => ['input', 'float','00118','2','хз'],
			"K1_T4" => ['input', 'float','00120','2','хз'],
			"K1_P4" => ['input', 'float','00122','2','хз'],
			"K1_PP" => ['input', 'int','00124','1','хз'],
			"K1_NStat" => ['input', 'int','00125','1','хз'],
			"K1_N1Type" => ['input', 'int','00126','1','хз'],
			"K1_N1Stat" => ['input', 'int','00127','1','хз'],
			"K1_N1pp" => ['input', 'int','00128','1','хз'],
			"K1_N2Type" => ['input', 'int','00129','1','хз'],
			"K1_N2Stat" => ['input', 'int','00130','1','хз'],
			"K1_N2pp" => ['input', 'int','00131','1','хз'],
			"K1_NRsetp" => ['input', 'float','00132','2','хз'],
			"K1_NRStat" => ['input', 'int','00134','1','хз'],
			"K1_Rsetp" => ['input', 'float','00135','2','хз'],
			"K1_RStat" => ['input', 'int','00137','1','хз'],
			"K1_Rpp" => ['input', 'int','00138','1','хз'],
			"K1_NPStat" => ['input', 'int','00139','1','хз'],
			"K1_r1" => ['input', 'long','00140','2','хз'],
			"K1_r2" => ['input', 'long','00142','2','хз'],
			"K1_r3" => ['input', 'long','00144','2','хз'],
			"K1_r4" => ['input', 'long','00146','2','хз'],
			"K2_Type" => ['input', 'int','00148','1','хз'],
			"K2_VCode" => ['input', 'long','00149','2','хз'],
			"K2_DCode" => ['input', 'int','00151','1','хз'],
			"K2_ADCode" => ['input', 'int','00152','1','хз'],
			"K2_OCode" => ['input', 'int','00153','1','хз'],
			"K2_ACode" => ['input', 'long','00154','2','хз'],
			"K2_Mode" => ['input', 'int','00156','1','хз'],
			"K2_GMode" => ['input', 'int','00157','1','хз'],
			"K2_GMin" => ['input', 'float','00158','2','хз'],
			"K2_GMax" => ['input', 'float','00160','2','хз'],
			"K2_G1x" => ['input', 'int','00162','1','хз'],
			"K2_G2x" => ['input', 'int','00163','1','хз'],
			"K2_G3x" => ['input', 'int','00164','1','хз'],
			"K2_G1y" => ['input', 'int','00165','1','хз'],
			"K2_G2y" => ['input', 'int','00166','1','хз'],
			"K2_G3y" => ['input', 'int','00167','1','хз'],
			"K2_Gbias" => ['input', 'float','00168','2','хз'],
			"K2_Gout" => ['input', 'float','00170','2','хз'],
			"K2_T5" => ['input', 'float','00172','2','хз'],
			"K2_P5" => ['input', 'float','00174','2','хз'],
			"K2_T6" => ['input', 'float','00176','2','хз'],
			"K2_P6" => ['input', 'float','00178','2','хз'],
			"K2_PP" => ['input', 'int','00180','1','хз'],
			"K2_NStat" => ['input', 'int','00181','1','хз'],
			"K2_N1Type" => ['input', 'int','00182','1','хз'],
			"K2_N1Stat" => ['input', 'int','00183','1','хз'],
			"K2_N1pp" => ['input', 'int','00184','1','хз'],
			"K2_N2Type" => ['input', 'int','00185','1','хз'],
			"K2_N2Stat" => ['input', 'int','00186','1','хз'],
			"K2_N2pp" => ['input', 'int','00187','1','хз'],
			"K2_NRsetp" => ['input', 'float','00188','2','хз'],
			"K2_NRStat" => ['input', 'int','00190','1','хз'],
			"K2_Rsetp" => ['input', 'float','00191','2','хз'],
			"K2_RStat" => ['input', 'int','00193','1','хз'],
			"K2_Rpp" => ['input', 'int','00194','1','хз'],
			"K2_NPStat" => ['input', 'int','00195','1','хз'],
			"K2_r1" => ['input', 'long','00196','2','хз'],
			"K2_r2" => ['input', 'long','00198','2','хз'],
			"K2_r3" => ['input', 'long','00200','2','хз'],
			"K2_r4" => ['input', 'long','00202','2','хз'],
			"Расход воды м^3/ч" => ['input', 'float','00204','2','хз'],
			"t4.1" => ['input', 'float','00206','2','хз'],
			"t6.1" => ['input', 'float','00208','2','хз'],
			"P4.1" => ['input', 'float','00210','2','хз'],
			"P2.1" => ['input', 'float','00212','2','хз'],
			"t3.2" => ['input', 'float','00214','2','хз'],
			"t5.2" => ['input', 'float','00216','2','хз'],
			"P3.2" => ['input', 'float','00218','2','хз'],
			"P1.2" => ['input', 'float','00220','2','хз'],
			"ModbusTCP: Статус дренажа" => ['input', 'int','00222','1','хз'],
			"ИПТ2" => ['input', 'float','00223','2','хз'],
			"ИПТ1" => ['input', 'float','00225','2','хз'],
			"Т2 гвс" => ['input', 'float','00227','2','Т2 температура в обратке подающего ГВС контура, сразу после теплообменника ГВС'],
			"Т1 отопление" => ['input', 'float','00229','2','Т1 температура в обратке подающего контура отопления'],
			# holding registers
			"AIN-TC-t(прямая)" => ['holding', 'float','41032','2','температура в подающем трубопроводе теплосети'],
			"AIN-TC-p(прямая)" => ['holding', 'float','41036','2','температура в подающем трубопроводе теплосети'],
			"AIN-TC-p(обратная)" => ['holding', 'float','41038','2','температура в подающем трубопроводе теплосети'],

			
	);
# Создаём коннект к БД
DBI->trace(1);
my $db_name='heatn';
my $host='172.17.10.2';
my $user='heatn_user';
my $pass='256t#48';
    my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host", $user, $pass,{AutoCommit => 1}) or die "Error connecting to database";

# Генерируем таблицы с modbus переменными
while (my ($row_name, $data) = each %mod_bus_hsh){
	#	my $arr_str = join (',',map {$_="\"$_\"";} @$data); #разбиваем запятыми и добавляем каждому элементу кавычки
	#	p $arr_str;
		#my $sql0 = "alter table test_node.modbus_vars ADD column $row_name varchar[] NULL;";
			my $sql01 = "INSERT INTO test_node.modbus_vars (var_name, registers, type, address, reg_numbers, descr) values ('$row_name', '$$data[0]', '$$data[1]', '$$data[2]', '$$data[3]', '$$data[4]');";
			#	my $sql1 = "alter table test_node.vars_data ADD column $row_name varchar NULL;";
	
			#$dbh->do($sql0)
			#		or die "My error column creation"; 
				$dbh->do($sql01)
					or die "My error insert data";
				#	$dbh->do($sql1)
				#	or die "My error column creation";
}
#
#my $client = Device::Modbus::TCP::Client->new(
#    host => '10.240.226.115',
#);
##print Dumper  $client;
#print "###################### host => 10.240.226.115 ##############################\n"; 
##my %final_data;     # {блок регистров  памяти => [address, data, var_type, reg_numbers, descr]}
#while (my($name, @data_arr) = each %mod_bus_hsh ){
#	#print Dumper $name;
#	#print $#data_arr;
#	#p $data_arr[0][2];
#	if ($data_arr[0][0] eq 'input'){
#		#print'##################### Читаем из input registers ######################'."\n";
#		my $req = $client->read_input_registers(                     # read_input_registers
#		    unit     => 1, # адрес модбас устройства (0-256) 
#		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
#		    quantity => $data_arr[0][3]  # размер регистра
#		);
#		#	p $req;
#		$client->send_request($req) || die "Send error: $!";
#		my $response = $client->receive_response;
#		#	$final_data{'read_input_registers'} = []
#		my $data_mir = $response->{message}->{values};
#		# приведение значения к типу
#		my $prep_data=();
#		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
#		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
#		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
#		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
#		# ---------------------------
#		print "registers_bank = $data_arr[0][0], $name = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#	print "-----------------------------------------------------------------------------\n";
#		#p $response;
#	} 
#	elsif ($data_arr[0][0] eq 'coil'){
#		#print'##################### Читаем из coils registers ######################'."\n";
#		my $req = $client->read_coils(                     # read_input_registers
#		    unit     => 1, # адрес модбас устройства (0-256) 
#		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
#		    quantity => $data_arr[0][3]  # размер регистра
#		);
#		#	p $req;
#		$client->send_request($req) || die "Send error: $!";
#		my $response = $client->receive_response;
#		my $data_mir = $response->{message}->{values};
#		# приведение значения к типу
#		my $prep_data=();
#		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
#		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
#		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
#		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
#		# ---------------------------
#		print "registers_bank = $data_arr[0][0], $name = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#print "$name = @$data_mir, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#print "-----------------------------------------------------------------------------\n";
#		#p $response;
#	} 
#	elsif ($data_arr[0][0] eq 'input_status'){
#		#print'##################### Читаем из input_status registers ######################'."\n";
#		my $req = $client->read_discrete_inputs(                     # read_input_registers
#		    unit     => 1, # адрес модбас устройства (0-256) 
#		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
#		    quantity => $data_arr[0][3]  # размер регистра
#		);
#		#	p $req;
#		$client->send_request($req) || die "Send error: $!";
#		my $response = $client->receive_response;
#		my $data_mir = $response->{message}->{values};
#		# приведение значения к типу
#		my $prep_data=();
#		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
#		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
#		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
#		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
#		# ---------------------------
#		print "registers_bank = $data_arr[0][0], $name = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#print "$name = @$data_mir, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#print "-----------------------------------------------------------------------------\n";
#		#p $response;
#	} 
#	elsif ($data_arr[0][0] eq 'holding'){
#		#print'##################### Читаем из holding registers ######################'."\n";
#		my $req = $client->read_holding_registers(                     # read_input_registers
#		    unit     => 1, # адрес модбас устройства (0-256) 
#		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
#		    quantity => $data_arr[0][3]  # размер регистра
#		);
#		#	p $req;
#		$client->send_request($req) || die "Send error: $!";
#		my $response = $client->receive_response;
#		my $data_mir = $response->{message}->{values};
#		# приведение значения к типу
#		my $prep_data=();
#		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
#		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
#		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
#		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
#		# ---------------------------
#		print "registers_bank = $data_arr[0][0], $name = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#print "$name = @$data_mir, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";# print Dumper $response->{values};
#		#print "-----------------------------------------------------------------------------\n";
#		#p $response;
#	} 
#
#}
#$client->disconnect;
#
##########################################################################################################
## фукнкция преобразования десятичного значения из 1 регистра modbus в значение целого типа int со знаком
##########################################################################################################
#sub prep_int{
#my $ref=shift;
#my ($dec)=@$ref;
#my $res; #сюда запишем отрицательное число
#my $bin = sprintf ("%b", $dec); # получаем бинарный вид числа
##my $str = unpack("B16", pack("N", $dec));
#			while (length($bin)<16){
#				$bin="0".$bin;
#			}
#if (substr($bin,0,1) eq '0')
#	{ return $dec;}
#	else{
#		my @bits = split(//, $bin); # переводим бинарное число в битовый массив
#		my $i = 0;
#		foreach (@bits){            # инверсия
#			if ($_){
#				$bits[$i]=0;
#			}
#			else {
#				$bits[$i]=1;
#			}
#			$i++;
#		}
#		$i =0;
#		my $cur = 1;
#		# далее прибавляем 1
#	do {
#		if ($bits[($#bits)-$i]==1){
#			if ($cur==1){
#				$bits[($#bits)-$i]=0;
#				$cur=1;
#			}
#		}else {
#			if ($cur==1){
#				$bits[($#bits)-$i]=1;
#				$cur=0;
#			}
#		}
#			$i++;
#			if ($i==16){$cur=0;}
#	} while $cur;
#		$res = join ('',@bits);
#		my $dec_res = oct('0b'.$res);
#		if ($dec_res != 0){$dec_res='-'.$dec_res;}
#	return $dec_res; 
#	}
#}
#
##########################################################################################################
## фукнкция преобразования десятичного значения из 2 регистра modbus в значение целого типа long со знаком
##########################################################################################################
#sub prep_long{
#my $ref=shift;
#my ($dec1, $dec2) = @$ref;
#my $res=''; # сюда сложим результат
#my $bin1 = sprintf ("%b", $dec1);
#my $bin2 = sprintf ("%b", $dec2);
#	# дополняем бинарники нулями
#			while (length($bin1)<16){
#				$bin1="0".$bin1;
#			}
#
#			while (length($bin2)<16){
#				$bin2="0".$bin2;
#			}
#my $bin_union = $bin2.$bin1;
#if (substr($bin_union,0,1) eq '0')
#	{ return  oct('0b'.$bin_union);}
#	else{
#		my @bits = split(//, $bin_union); # переводим бинарное число в битовый массив
#		my $i = 0;
#		foreach (@bits){            # инверсия
#			if ($_){
#				$bits[$i]=0;
#			}
#			else {
#				$bits[$i]=1;
#			}
#			$i++;
#		}
#		$i =0;
#		my $cur = 1;
#		# далее прибавляем 1
#	do {
#		if ($bits[($#bits)-$i]==1){
#			if ($cur==1){
#				$bits[($#bits)-$i]=0;
#				$cur=1;
#			}
#		}else {
#			if ($cur==1){
#				$bits[($#bits)-$i]=1;
#				$cur=0;
#			}
#		}
#			$i++;
#			if ($i==32){$cur=0;}
#	} while $cur;
#		$res = join ('',@bits);
#		my $dec_res = oct('0b'.$res);
#		if ($dec_res != 0){$dec_res='-'.$dec_res;}
#	return $dec_res; 
#	}
#}
#
##########################################################################################################
## фукнкция преобразования массива значений из 1 регистра modbus в значение типа bool 
##########################################################################################################
#sub prep_bool{
#my $ref=shift;
#return @$ref[0];
#}
#
##########################################################################################################
## функция преобразования десятичных значений из 2 регистров modbus в значения вещественного типа со знаком
##########################################################################################################
#sub prep_float {
#my $ref=shift;
#my ($dec1, $dec2) = @$ref;
#my $res=''; # сюда сложим результат
#my $bin1 = sprintf ("%b", $dec1);
#my $bin2 = sprintf ("%b", $dec2);
#	# дополняем бинарники нулями
#			while (length($bin1)<16){
#				$bin1="0".$bin1;
#			}
#
#			while (length($bin2)<16){
#				$bin2="0".$bin2;
#			}
#my $bin_union = $bin2.$bin1;
#if ((substr $bin_union,0,1) eq '1') {$res='-';}  # установка знака числа
#my $mantiss_bin = substr $bin_union,1,8;
#my $num_mantiss = bin2dec($mantiss_bin)-127;
#my $int_part_num = bin2dec('1'.substr($bin_union,9,$num_mantiss));
#my $fract_part_num = bin2dec(substr($bin_union,9+$num_mantiss,23-$num_mantiss));
#return $res ="$res"."$int_part_num"."."."$fract_part_num";
#}
#
#sub dec2bin {
#my $str = unpack("B32", pack("N", shift));
#return $str;
#}
#
#sub bin2dec {
#return unpack("N", pack("B32", substr("0" x 32 . shift, -32)));
#}
