#!/usr/bin/env perl
#===============================================================================
#
#         FILE: modbusfn.pl
#
#        USAGE: ./modbusfn.pl  
#
#  DESCRIPTION: Модуль содержит функции для скриптов работающих с даннными по модбас с ПЛК SMH4 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 25.02.2020 11:31:25
#     REVISION: ---
#===============================================================================
package modbusfn; #module
our $VERSION = '1.0'; #module 

use strict;
use warnings;
use utf8;
use Device::Modbus::TCP::Client;
use Data::Dumper;
use Data::Printer;
binmode(STDOUT,':utf8');
use 5.010;
use Time::Piece;
use AnyEvent;
use DBI();
use DBD::Pg;

# общие переменные
my $hostname= '10.240.226.115'; # адрес ПЛК

# Создаём коннект к БД
sub db_connect{
DBI->trace(1);
my $db_name='heatn';
my $host='172.17.10.2';
my $user='heatn_user';
my $pass='256t#48';
    my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host", $user, $pass,{AutoCommit => 1}) or die "Error connecting to database";
    return $dbh;
}

sub db_close{
my $db=shift;
return $db->disconnect;
}

#----- Забираем из базы таблицу со всеми переменнными modbus -----------------
sub db_get_modbus_vars{
my $dbh = db_connect;
my $sth=$dbh->prepare("SELECT id, var_name, registers, type, address, reg_numbers, descr from test_node.modbus_vars");
$sth-> execute;
my ($id, $var_name, $registers, $type, $address, $reg_numbers, $descr);
$sth->bind_columns(\$id, \$var_name, \$registers, \$type, \$address, \$reg_numbers, \$descr);
my %mod_bus_hsh;
while ($sth->fetch) {
	$mod_bus_hsh{$id}=[$registers, $type, $address, $reg_numbers, $descr, $var_name];
}
db_close($dbh);
return \%mod_bus_hsh;
}

#----- Забираем из базы таблицу с переменнными modbus для записи-----------------
sub db_get_modbus_wr_vars{
my $dbh = db_connect;
my $sth=$dbh->prepare("SELECT id, var_name, registers, type, address, reg_numbers, descr FROM test_node.modbus_vars WHERE tr_status = 'rw';");
$sth-> execute;
my ($id, $var_name, $registers, $type, $address, $reg_numbers, $descr);
$sth->bind_columns(\$id, \$var_name, \$registers, \$type, \$address, \$reg_numbers, \$descr);
my %mod_bus_hsh;
while ($sth->fetch) {
	$mod_bus_hsh{$id}=[$registers, $type, $address, $reg_numbers, $descr, $var_name];
}
db_close($dbh);
return \%mod_bus_hsh;
}

#----- Забираем из базы таблицу с переменнными modbus только для чтения-----------------
sub db_get_modbus_read_vars{
my $dbh = db_connect;
my $sth=$dbh->prepare("SELECT id, var_name, registers, type, address, reg_numbers, descr FROM test_node.modbus_vars WHERE tr_status = 'read';");
$sth-> execute;
my ($id, $var_name, $registers, $type, $address, $reg_numbers, $descr);
$sth->bind_columns(\$id, \$var_name, \$registers, \$type, \$address, \$reg_numbers, \$descr);
my %mod_bus_hsh;
while ($sth->fetch) {
	$mod_bus_hsh{$id}=[$registers, $type, $address, $reg_numbers, $descr, $var_name];
}
db_close($dbh);
return \%mod_bus_hsh;
}

#---- Берем временную метку ----------------------
sub get_time{
my $tm = Time::Piece -> new;
my $time_mark = $tm -> datetime;
return \$time_mark;
}

#---- Работаем с модбас -----------------------------
sub get_modbus_data{
my $hsh=shift;
my %mod_bus_hsh=%$hsh;
my $time_mark = get_time;
my $client = Device::Modbus::TCP::Client->new(
    host => $hostname,
);
#print Dumper  $client;
print "###################### host => $hostname ##############################\n"; 
#my %final_data;     # {блок регистров  памяти => [address, data, var_type, reg_numbers, descr]}
my %var_data; # хэш модбас данных, полученных из регистров
while (my($var_id, @data_arr) = each %mod_bus_hsh ){
	#print Dumper $var_id;
	#print $#data_arr;
	#p $data_arr[0][2];
	if ($data_arr[0][0] eq 'input'){
		#print'##################### Читаем из input registers ######################'."\n";
		my $req = $client->read_input_registers(                     # read_input_registers
		    unit     => 1, # адрес модбас устройства (0-256) 
		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
		    quantity => $data_arr[0][3]  # размер регистра
		);
		#	p $req;
		$client->send_request($req) || die "Send error: $!";
		my $response = $client->receive_response;
		#	$final_data{'read_input_registers'} = []
		my $data_mir = $response->{message}->{values};
		# приведение значения к типу
		my $prep_data=();
		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
		# ---------------------------
		print "registers_bank = $data_arr[0][0], $var_id = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";
		$var_data{$$time_mark}{$var_id} = [$prep_data, "read"];
	} 
	elsif ($data_arr[0][0] eq 'coil'){
		#print'##################### Читаем из coils registers ######################'."\n";
		my $req = $client->read_coils(                     # read_input_registers
		    unit     => 1, # адрес модбас устройства (0-256) 
		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
		    quantity => $data_arr[0][3]  # размер регистра
		);
		#	p $req;
		$client->send_request($req) || die "Send error: $!";
		my $response = $client->receive_response;
		my $data_mir = $response->{message}->{values};
		# приведение значения к типу
		my $prep_data=();
		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
		# ---------------------------
		print "registers_bank = $data_arr[0][0], $var_id = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";
		$var_data{$$time_mark}{$var_id} = [$prep_data, "read"];
	} 
	elsif ($data_arr[0][0] eq 'input_status'){
		#print'##################### Читаем из input_status registers ######################'."\n";
		my $req = $client->read_discrete_inputs(                     # read_input_registers
		    unit     => 1, # адрес модбас устройства (0-256) 
		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
		    quantity => $data_arr[0][3]  # размер регистра
		);
		#	p $req;
		$client->send_request($req) || die "Send error: $!";
		my $response = $client->receive_response;
		my $data_mir = $response->{message}->{values};
		# приведение значения к типу
		my $prep_data=();
		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
		# ---------------------------
		print "registers_bank = $data_arr[0][0], $var_id = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";
		$var_data{$$time_mark}{$var_id} = [$prep_data, "read"];
	} 
	elsif ($data_arr[0][0] eq 'holding'){
		#print'##################### Читаем из holding registers ######################'."\n";
		my $req = $client->read_holding_registers(                     # read_input_registers
		    unit     => 1, # адрес модбас устройства (0-256) 
		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
		    quantity => $data_arr[0][3]  # размер регистра
		);
		#	p $req;
		$client->send_request($req) || die "Send error: $!";
		my $response = $client->receive_response;
		my $data_mir = $response->{message}->{values};
		# приведение значения к типу
		my $prep_data=();
		if ($data_arr[0][1] eq 'bool'){$prep_data = prep_bool($data_mir)}
		elsif ($data_arr[0][1] eq 'int'){$prep_data = prep_int($data_mir)}
		elsif ($data_arr[0][1] eq 'long'){$prep_data = prep_long($data_mir)}
		elsif ($data_arr[0][1] eq 'float'){$prep_data = prep_float($data_mir)}
		# ---------------------------
		print "registers_bank = $data_arr[0][0], $var_id = |$prep_data|, var_type = $data_arr[0][1], registers = $data_arr[0][3] address = $data_arr[0][2] descr = $data_arr[0][4]\n";
		$var_data{$$time_mark}{$var_id} = [$prep_data, "read"]; 
	} 

}
$client->disconnect;
return \%var_data;
}
#---------------------------------------------

#---- Устанавливаем значение модбас переменных ----
sub set_modbus_data {
my $hsh = shift;
my %mod_bus_hsh=%$hsh;
my $time_mark = get_time;
my $client = Device::Modbus::TCP::Client->new(
    host => $hostname,
);
my %var_data; # хэш модбас данных, полученных из регистров
while (my($var_id, @data_arr) = each %mod_bus_hsh ){
	#print Dumper $var_id;
	#print $#data_arr;
	#p $data_arr[0][2];
	if ($data_arr[0][0] eq 'coil'){
		#print'##################### Читаем из coils registers ######################'."\n";
		my $req = $client->write_single_coil(                     # write coil 
		    unit     => 1, # адрес модбас устройства (0-256) 
		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
		    value => $data_arr[0][6]  # значение регистра
		);
		#	p $req;
		$client->send_request($req) || die "Send error: $!";
		my $response = $client->receive_response;
		$var_data{$$time_mark}{$var_id} = [$data_arr[0][6], "write"]; 
	} 
	elsif ($data_arr[0][0] eq 'holding'){
		#print'##################### Читаем из holding registers ######################'."\n";
		my $req = $client->write_single_register(               # write holding 
		    unit     => 1, # адрес модбас устройства (0-256) 
		    address  => $data_arr[0][2], # адрес регистра модбас устройства в десятичном виде
		    value => $data_arr[0][6]  #  значение регистра

		);
		#	p $req;
		$client->send_request($req) || die "Send error: $!";
		my $response = $client->receive_response;
		$var_data{$$time_mark}{$var_id} = [$data_arr[0][6], "write"]; 
	} 

}
$client->disconnect;
return \%var_data;
}
#---------------------------------------------
#---- Записываем в БД с учётом статуса переменной (read/write)----
sub db_insert_data{
my $ref=shift;
my %data_hsh = %$ref;
my @time = keys %data_hsh; 
my $dbh=db_connect;
my %hsh2 = %{$data_hsh{$time[0]}};
p %hsh2;
	while (my ($id, @val) = each %hsh2){
		p $id;
		p @val;
		my $sql = "INSERT INTO test_node.vars_data (time, var_id, data, tr_status) values ('$time[0]', '$id', '$val[0][0]','$val[0][1]');";	
		$dbh->do($sql)	or die "error insert data in vars_data!";

	}
db_close($dbh);
return "insertion completed.";
}
#----------------------------------------------

#########################################################################################################
# фукнкция преобразования десятичного значения из 1 регистра modbus в значение целого типа int со знаком
#########################################################################################################
sub prep_int{
my $ref=shift;
my ($dec)=@$ref;
my $res; #сюда запишем отрицательное число
my $bin = sprintf ("%b", $dec); # получаем бинарный вид числа
#my $str = unpack("B16", pack("N", $dec));
			while (length($bin)<16){
				$bin="0".$bin;
			}
if (substr($bin,0,1) eq '0')
	{ return $dec;}
	else{
		my @bits = split(//, $bin); # переводим бинарное число в битовый массив
		my $i = 0;
		foreach (@bits){            # инверсия
			if ($_){
				$bits[$i]=0;
			}
			else {
				$bits[$i]=1;
			}
			$i++;
		}
		$i =0;
		my $cur = 1;
		# далее прибавляем 1
	do {
		if ($bits[($#bits)-$i]==1){
			if ($cur==1){
				$bits[($#bits)-$i]=0;
				$cur=1;
			}
		}else {
			if ($cur==1){
				$bits[($#bits)-$i]=1;
				$cur=0;
			}
		}
			$i++;
			if ($i==16){$cur=0;}
	} while $cur;
		$res = join ('',@bits);
		my $dec_res = oct('0b'.$res);
		if ($dec_res != 0){$dec_res='-'.$dec_res;}
	return $dec_res; 
	}
}

#########################################################################################################
# фукнкция преобразования десятичного значения из 2 регистра modbus в значение целого типа long со знаком
#########################################################################################################
sub prep_long{
my $ref=shift;
my ($dec1, $dec2) = @$ref;
my $res=''; # сюда сложим результат
my $bin1 = sprintf ("%b", $dec1);
my $bin2 = sprintf ("%b", $dec2);
	# дополняем бинарники нулями
			while (length($bin1)<16){
				$bin1="0".$bin1;
			}

			while (length($bin2)<16){
				$bin2="0".$bin2;
			}
my $bin_union = $bin2.$bin1;
if (substr($bin_union,0,1) eq '0')
	{ return  oct('0b'.$bin_union);}
	else{
		my @bits = split(//, $bin_union); # переводим бинарное число в битовый массив
		my $i = 0;
		foreach (@bits){            # инверсия
			if ($_){
				$bits[$i]=0;
			}
			else {
				$bits[$i]=1;
			}
			$i++;
		}
		$i =0;
		my $cur = 1;
		# далее прибавляем 1
	do {
		if ($bits[($#bits)-$i]==1){
			if ($cur==1){
				$bits[($#bits)-$i]=0;
				$cur=1;
			}
		}else {
			if ($cur==1){
				$bits[($#bits)-$i]=1;
				$cur=0;
			}
		}
			$i++;
			if ($i==32){$cur=0;}
	} while $cur;
		$res = join ('',@bits);
		my $dec_res = oct('0b'.$res);
		if ($dec_res != 0){$dec_res='-'.$dec_res;}
	return $dec_res; 
	}
}

#########################################################################################################
# фукнкция преобразования массива значений из 1 регистра modbus в значение типа bool 
#########################################################################################################
sub prep_bool{
my $ref=shift;
return @$ref[0];
}

#########################################################################################################
# функция преобразования десятичных значений из 2 регистров modbus в значения вещественного типа со знаком
#########################################################################################################
sub prep_float {
my $ref=shift;
my ($dec1, $dec2) = @$ref;
my $res=''; # сюда сложим результат
my $int_part_num; # целая часть числа
my $fract_part_num; # дробная часть числа
my $bin1 = sprintf ("%b", $dec1);
my $bin2 = sprintf ("%b", $dec2);
	# дополняем бинарники нулями
			while (length($bin1)<16){
				$bin1="0".$bin1;
			}

			while (length($bin2)<16){
				$bin2="0".$bin2;
			}
my $bin_union = $bin2.$bin1;
if ((substr $bin_union,0,1) eq '1') {$res='-';}  # установка знака числа
my $mantiss_bin = substr $bin_union,1,8;
my $num_mantiss = bin2dec($mantiss_bin)-127;
my @num_fragment = split(//, substr($bin_union,9));
unshift (@num_fragment,'1'); # дополняем наше 2-ичное число сокращённой 1
p @num_fragment;
if ($num_mantiss == 0){ 
	$int_part_num = 0;
	}
	else{
		my $l_res=0;
		foreach (@num_fragment){
			p $l_res;
		$l_res = $l_res+$_*(2**$num_mantiss);
		$num_mantiss--;
		p $num_mantiss;
		if ($num_mantiss == 0){
			$int_part_num = $l_res;
			$l_res =0;
			}
		$fract_part_num = $l_res;
		}
	}
	my $num = $int_part_num+$fract_part_num;
return $res = "$res$num";
}

#sub dec2bin {
#my $str = unpack("B32", pack("N", shift));
#return $str;
#}

sub bin2dec {
return unpack("N", pack("B32", substr("0" x 32 . shift, -32)));
}
##############################################################
1; #module

