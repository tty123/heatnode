#!/usr/bin/env perl
#===============================================================================
#
#         FILE: modbus_data_transmitter.pl
#
#        USAGE: ./modbus_data_transmitter.pl  
#
#  DESCRIPTION: Скрипт устанавливает по сети значение модбас переменных в ПЛК SMH4 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 25.02.2020 11:11:29
#     REVISION: 26.02.2020
#===============================================================================

use strict;
use warnings;
use utf8;
use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use modbusfn;
use 5.010;
binmode(STDOUT,':utf8');
use Data::Printer;
use Data::Dumper;

my $ref_whsh = modbusfn::db_get_modbus_wr_vars; #!
if (defined $ref_whsh){ say "Нет значений в базе!"}
p $ref_whsh;
$ref_whsh->{'218'}[6]=0;# устанавливаем бит
$ref_whsh->{'219'}[6]=0;# устанавливаем бит
$ref_whsh->{'220'}[6]=0;# устанавливаем бит
p $ref_whsh;
# Изменяем значения модбас переменных
my $ref_dhsh = modbusfn::set_modbus_data ($ref_whsh);
say "____________________";
p $ref_dhsh;
say modbusfn::db_insert_data($ref_dhsh); # записываем новые значения переменных в базу
 
