#!/usr/bin/env perl
#===============================================================================
#
#         FILE: modbus_test.pl
#
#        USAGE: ./modbus_test.pl  
#
#  DESCRIPTION: Скрипт для отработки соединения с ПЛК SMH4
#               По modbus получаем значение переменной из тестового проекта для ПЛК
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 27.08.2019 14:29:23
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Device::Modbus::TCP::Client;
use Data::Dumper;
use Data::Printer;
use v5.10;
 
my $client = Device::Modbus::TCP::Client->new(
    host => '192.168.1.40',
);
p $client;
 
my $req = $client->read_input_registers(                     # read_input_registers
    unit     => 1, # адрес модбас устройства (0-256) 
    address  => 40960, # адрес регистра модбас устройства в десятичном виде
    quantity => 1  # размер регистра
);
p $req;
say Dumper $req;
$client->send_request($req) || die "Send error: $!";

my $r = $client->write_single_register(         # пишем в holding registers (карта адресов в новом виде)
    unit     => 1,
    address  => 00000,
    value    => 248 ); 

p $r;
say Dumper $r;
$client->send_request($r) || die "Send error: $!";
my $response = $client->receive_response;
say Dumper $response;
 
$client->disconnect;



