#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_unit_sensor_v3_1m.pl
#
#        USAGE: ./test_unit_sensor_v3_1m.pl  
#
#  DESCRIPTION: Тестирование формулы работы блока Sensor v3.1 проекта теплового узла
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 20.02.2020 12:03:34
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;

my $ADCinput = 110;
my $ADCmin_real = 100;
my $ADCmax_real = 139;
my $Tmin_real = 0;
my $Tmax_real = 100;

my $res = ($ADCinput-$ADCmin_real)*($Tmax_real-$Tmin_real)/($ADCmax_real-$ADCmin_real)+$Tmin_real;
say "\$ADCinput=$ADCinput"; 
say "\$ADCmin_real=$ADCmin_real"; 
say "\$ADCmax_real=$ADCmax_real"; 
say "\$Tmin_real=$Tmin_real";
say "\$Tmax_real=$Tmax_real";
say "\$res = $res";
